sonoran is a strace/truss output browser.
For the first version, the focus is on file-dependent operations (open, stat, execv)
(c)2013-14 Alexander Haas

Licensed under GPLv3
