/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
//#include <QTranslator>
//#include <QString>
#include <map>
#include "syscallDialog.hpp"
#include "SyscallDetectorFactory.hpp"
#include "SonoranMainLogic.hpp"
#include "LineReader.hpp"


//debug only
#include <iostream>

int main(int argc, char* argv[])
{
    // if arg passed, its the name of the config file
    std::string straceFilename = "";
    if (argc == 2)  // first arg is executeable, second is first "real" argument
    {
        straceFilename=argv[1];
    }
    else
    {
        // in the first version, no pulldown menu to load a file exists,
        // so the strace file has to be passed as (the one and only) parameter
        std::cerr << "Usage: sonoran <stracefile>" << std::endl;
        return(1);
    }



    // initial file treatment
    // just as first quit shot, to be refined/refactored later
    CLineReader* lineReader = new CLineReader(QString(straceFilename.c_str()));
    CSyscallDetectorFactory* detectorFactory = new CSyscallDetectorFactory(lineReader);


    std::map<CSyscallDetectorFactory::ESyscallType,QString> availableSyscalls =
        detectorFactory->getAvailableSyscalls();


    QApplication sonoranApp(argc, argv);
    sonoranApp.setStyle("cleanlooks");

    CSyscallDialog* syscallDialog = new CSyscallDialog(lineReader, availableSyscalls);

    SonoranMainLogic* mainLogic = new SonoranMainLogic(lineReader,
                                                       detectorFactory,
                                                       syscallDialog);



    syscallDialog->setWindowTitle("Sonoran strace/truss output browser v0.1");
    syscallDialog->show();
    return(sonoranApp.exec());







    //delete syscallDialog;

    //return(0);
}
