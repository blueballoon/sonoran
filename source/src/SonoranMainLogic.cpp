/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SonoranMainLogic.hpp"
#include "syscallDialog.hpp"

// debug only
#include <iostream>



SonoranMainLogic::SonoranMainLogic(CLineReader* lineReader,
                                   CSyscallDetectorFactory* syscallDetectorFactory,
                                   CSyscallDialog* syscallDialog)
{
    // fill gui with start values

    // set signal/slot connections
    connect(syscallDialog, SIGNAL(signalOccurrencySelected(unsigned int)),
            syscallDialog, SLOT(slotSetCurrentOccurrencyLine(unsigned int)));

    connect(syscallDialog, SIGNAL(signalSyscallTypeSelected(const QString&)),
            this, SLOT(slotHandleNewSyscallType(const QString&)));
}



void SonoranMainLogic::slotHandleNewSyscallType(const QString& syscallType)
{
    std::cerr << "current syscall type " << syscallType.toStdString() << std::endl;
}
