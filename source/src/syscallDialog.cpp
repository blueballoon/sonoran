/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtGui>
#include "syscallDialog.hpp"
#include "LineReader.hpp"

// debug only
#include <iostream>


CSyscallDialog::CSyscallDialog(
		CLineReader* lineReader,
		const std::map<CSyscallDetectorFactory::ESyscallType,QString>& availableSyscalls,
		QWidget *parent): QDialog(parent)
{
    createWidgets();
    layoutWidgets();

    fillTypeComboFromFactoryCapabilities(availableSyscalls,m_syscallTypeCombo);

    // fill trace-textview with text from linereader
}



void CSyscallDialog::slotSetCurrentOccurrencyLine(unsigned int occurrencyLine)
{
    std::cerr << "current occurrency line to display = " << occurrencyLine << std::endl;
}



void CSyscallDialog::createWidgets()
{
    // create button line widgets
    m_syscallTypeLabel = new QLabel(tr("Syscall type:"));
    m_syscallTypeCombo = new QComboBox; //currently empty;
    m_syscallTypeLabel->setBuddy(m_syscallTypeCombo);

    m_quitButton = new QPushButton(tr("Quit"));


    // create tableview and textview widgets
    m_syscallTable = new QTableView();  // todo: set header line, nr cols etc

    m_traceTextView = new QTextEdit();
}



void CSyscallDialog::layoutWidgets()
{
    // button line
    m_ButtonLineLayout = new QHBoxLayout;
    m_ButtonLineLayout->addWidget(m_syscallTypeLabel);
    m_ButtonLineLayout->addWidget(m_syscallTypeCombo);
    m_ButtonLineLayout->addStretch();
    m_ButtonLineLayout->addWidget(m_quitButton);


    // table and textview with vertical splitter
    m_tableTextSplitter = new QSplitter(Qt::Vertical);
    m_tableTextSplitter->addWidget(m_syscallTable);
    m_tableTextSplitter->addWidget(m_traceTextView);
    m_tableTextSplitter->setStretchFactor(1,1);

    // group them togheter with a vertical layout
    m_dialogLayout = new QVBoxLayout;
    m_dialogLayout->addLayout(m_ButtonLineLayout);
    m_dialogLayout->addWidget(m_tableTextSplitter);

    // activate it
    setLayout(m_dialogLayout);
}



void CSyscallDialog::fillTypeComboFromFactoryCapabilities(
		const std::map<CSyscallDetectorFactory::ESyscallType,QString>& availableSyscalls,
        QComboBox* targetCombo)
{
    // not impl yet
	for(std::map<CSyscallDetectorFactory::ESyscallType,QString>::const_iterator syscallIter = availableSyscalls.begin();
			syscallIter != availableSyscalls.end(); ++syscallIter)
	{
		QString currSyscallText=(*syscallIter).second;
		targetCombo->addItem(currSyscallText);
	}

}
