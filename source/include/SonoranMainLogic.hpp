/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SONORANMAINLOGIC_HPP__
#define __SONORANMAINLOGIC_HPP__

#include <QObject>

// forwards
class CSyscallDialog;
class CSyscallDetectorFactory;
class CLineReader;


class SonoranMainLogic: public QObject
{
    Q_OBJECT

public:
    SonoranMainLogic(CLineReader*, CSyscallDetectorFactory*, CSyscallDialog*);


private slots:
    void slotHandleNewSyscallType(const QString& syscallType);
    //void slotHandleNewSyscallOccurrency(unsigned int occurrencyId);


/*
private:
    CLineReader* m_lineReader;
    CSyscallDetectorFactory* m_syscallDetectorFactory;
    CSyscallDialog* m_syscallDialog;
*/
};


#endif
