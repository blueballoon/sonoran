/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SYSCALLDIALOG_HPP__
#define __SYSCALLDIALOG_HPP__

#include <QDialog>
#include <QString>
#include <string>
#include <list>
#include <map>
#include "SyscallDetectorFactory.hpp"  // needed for ESyscallType

// forwards
class QLabel;
class QPushButton;
class QComboBox;
class QTableView;
class QTextEdit;
class QSplitter;
class QVBoxLayout;
class QHBoxLayout;
class CLineReader;

// the view


class CSyscallDialog : public QDialog
{
        Q_OBJECT
public:
        CSyscallDialog(CLineReader* lineReader, const std::map<CSyscallDetectorFactory::ESyscallType,QString>& availableSyscalls,
        		QWidget *parent = 0);
       // void setOccurrencyTable();
       // void setTraceOutputContent();


signals:
        void signalOccurrencySelected(unsigned int occurrencyId);
        void signalSyscallTypeSelected(const QString& syscallType);


public slots:
        void slotSetCurrentOccurrencyLine(unsigned int occurrencyLine);


private: // methods
        void createWidgets();
        void layoutWidgets();
        void fillTypeComboFromFactoryCapabilities(
        		const std::map<CSyscallDetectorFactory::ESyscallType,QString>& availableSyscalls,
                QComboBox* targetCombo);
private: // GUI Attributes

        // button line 1
        QLabel* m_syscallTypeLabel;
        QComboBox* m_syscallTypeCombo;
        QPushButton* m_quitButton;
        QHBoxLayout* m_ButtonLineLayout;

        // syscall table and trace output textview area
        QTableView* m_syscallTable;
        QTextEdit* m_traceTextView;
        QSplitter* m_tableTextSplitter;

        // put them together with a vertical layout
        QVBoxLayout* m_dialogLayout;
};

#endif
