/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "SyscallOccurrences.hpp"
#include "SyscallDetector.hpp"
#include "LineReader.hpp"
#include <QRegExp>
#include <stdexcept>



CSyscallOccurrences::CSyscallOccurrences(CLineReader* currStraceReader,
        CSyscallDetector* currSyscallDetector):
        m_straceReader(currStraceReader),
        m_syscallDetector(currSyscallDetector)
{

    m_syscallOccurrenceLineNumbers.clear();
    unsigned int nrInputLines = currStraceReader->getNrOfLines();
    const QRegExp* syscallRegExp = currSyscallDetector->getSyscallRegexp();

    // find all lines matching the given syscall in the given input file
    for(unsigned int currLineNr = 1; currLineNr <= nrInputLines; ++currLineNr)
    {
        QString currLineString = currStraceReader->getLine(currLineNr);
        if (syscallRegExp->exactMatch(currLineString))
        {
            m_syscallOccurrenceLineNumbers.insert(currLineNr);
        }
    }
}



std::set<unsigned int> CSyscallOccurrences::getSyscallOccurrenceLineNumbers()
{
    return(m_syscallOccurrenceLineNumbers);
}



QString CSyscallOccurrences::getSyscallResult(unsigned int straceLineNr)
{
    unsigned int resultPos = m_syscallDetector->getResultPos();
    QString captionString = getCaptionString(straceLineNr, resultPos);
    return(captionString);
}



QString CSyscallOccurrences::getSyscallPath(unsigned int straceLineNr)
{
    unsigned int pathPos = m_syscallDetector->getPathPos();
    QString captionString = getCaptionString(straceLineNr, pathPos);
    return(captionString);
}



// empty string if no pid available
QString CSyscallOccurrences::getSyscallPid(unsigned int straceLineNr)
{
    unsigned int pidPos = m_syscallDetector->getPidPos();
    QString captionString = getCaptionString(straceLineNr, pidPos);
    return(captionString);
}



QString CSyscallOccurrences::getCaptionString(unsigned int straceLineNr, unsigned int captionPosition)
{
    // check if a valid line number was given
    if (m_syscallOccurrenceLineNumbers.count(straceLineNr)==0)
    {
        throw(std::invalid_argument("Invalid line number for the syscall requested"));
    }

    // return immediately with an empty string if caption 0 is requested
    if (0==captionPosition)
    {
        return("");
    }

    const QRegExp* syscallRegExp = m_syscallDetector->getSyscallRegexp();

    QString currLineString = m_straceReader->getLine(straceLineNr);
    if (syscallRegExp->exactMatch(currLineString))
    {
        QString caption = syscallRegExp->cap(captionPosition);
        return(caption);
    }
    else
    {
        // something went wrong if the regex doesnt match
        throw(std::runtime_error("Syscall regex doesnt match although line number is correct"));
    }
}


