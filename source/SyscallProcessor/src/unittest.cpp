/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtTest/QtTest>
#include <iostream>
#include <set>
#include "LineReader.hpp"
#include "unittest_LineReaderMock.hpp"
#include "SyscallDetectorFactory.hpp"
#include "SyscallDetector.hpp"
#include "SyscallOccurrences.hpp"

class TestSyscallDetectorFactory: public QObject
{
    Q_OBJECT
    private slots:
        void testConstruction_plain_big();
        void testConstruction_pid();
        void testConstruction_pid_abstime();
        void testConstruction_pid_micros();
        void testConstruction_pid_epoch();
        void testOpenDetector();
        void testStatDetector();
        void testExecvDetector();
        void testAccessDetector();
        void testSyscallList();
};

void TestSyscallDetectorFactory::testConstruction_plain_big()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_plain_big();
    CSyscallDetectorFactory testedFactory = CSyscallDetectorFactory(lineReaderMock);
    QCOMPARE(testedFactory.m_hasPidColumn, false);
    QCOMPARE(testedFactory.m_hasTimeOfDayCol, false);
    QCOMPARE(testedFactory.m_hasTimeOfDayMicroCol, false);
    QCOMPARE(testedFactory.m_hasTimeOfDayEpochCol, false);
    delete lineReaderMock;
}

void TestSyscallDetectorFactory::testConstruction_pid()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_pid();
    CSyscallDetectorFactory testedFactory = CSyscallDetectorFactory(lineReaderMock);
    QCOMPARE(testedFactory.m_hasPidColumn, true);
    QCOMPARE(testedFactory.m_hasTimeOfDayCol, false);
    QCOMPARE(testedFactory.m_hasTimeOfDayMicroCol, false);
    QCOMPARE(testedFactory.m_hasTimeOfDayEpochCol, false);
    delete lineReaderMock;
}

void TestSyscallDetectorFactory::testConstruction_pid_abstime()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_pid_abstime();
    CSyscallDetectorFactory testedFactory = CSyscallDetectorFactory(lineReaderMock);
    QCOMPARE(testedFactory.m_hasPidColumn, true);
    QCOMPARE(testedFactory.m_hasTimeOfDayCol, true);
    QCOMPARE(testedFactory.m_hasTimeOfDayMicroCol, false);
    QCOMPARE(testedFactory.m_hasTimeOfDayEpochCol, false);
    delete lineReaderMock;
}
 
void TestSyscallDetectorFactory::testConstruction_pid_micros()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_pid_micros();
    CSyscallDetectorFactory testedFactory = CSyscallDetectorFactory(lineReaderMock);
    QCOMPARE(testedFactory.m_hasPidColumn, true);
    QCOMPARE(testedFactory.m_hasTimeOfDayCol, false);
    QCOMPARE(testedFactory.m_hasTimeOfDayMicroCol, true);
    QCOMPARE(testedFactory.m_hasTimeOfDayEpochCol, false);
    delete lineReaderMock;
}
 
void TestSyscallDetectorFactory::testConstruction_pid_epoch()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_pid_epoch();
    CSyscallDetectorFactory testedFactory = CSyscallDetectorFactory(lineReaderMock);
    QCOMPARE(testedFactory.m_hasPidColumn, true);
    QCOMPARE(testedFactory.m_hasTimeOfDayCol, false);
    QCOMPARE(testedFactory.m_hasTimeOfDayMicroCol, false);
    QCOMPARE(testedFactory.m_hasTimeOfDayEpochCol, true);
    delete lineReaderMock;
}

void TestSyscallDetectorFactory::testOpenDetector()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_plain_big();
    CSyscallDetectorFactory testedFactory = CSyscallDetectorFactory(lineReaderMock);
    CSyscallDetector* openDetector = testedFactory.createDetector(CSyscallDetectorFactory::open);
    QVERIFY(0!=openDetector);

    const QRegExp* openRegexp = openDetector->getSyscallRegexp();

    // Mock plain big has open commands in line 5, 9, 17
    QCOMPARE(openRegexp->exactMatch(lineReaderMock->getLine(5)), true);
    QCOMPARE(openRegexp->exactMatch(lineReaderMock->getLine(9)), true);
    QCOMPARE(openRegexp->exactMatch(lineReaderMock->getLine(17)), true);

    QCOMPARE(openRegexp->exactMatch(lineReaderMock->getLine(6)), false);
    QCOMPARE(openRegexp->exactMatch(lineReaderMock->getLine(7)), false);
    QCOMPARE(openRegexp->exactMatch(lineReaderMock->getLine(8)), false);


    // check positions
    QCOMPARE(openDetector->getPathPos(),1U);
    QCOMPARE(openDetector->getResultPos(),3U);
    QCOMPARE(openDetector->getPidPos(),0U);

    delete lineReaderMock;
    delete openDetector;
}


void TestSyscallDetectorFactory::testStatDetector()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_pid_stat64_access();
    CSyscallDetectorFactory testedFactory = CSyscallDetectorFactory(lineReaderMock);
    CSyscallDetector* statDetector = testedFactory.createDetector(CSyscallDetectorFactory::stat);
    QVERIFY(0!=statDetector);

    const QRegExp* statRegexp = statDetector->getSyscallRegexp();

    //qDebug() << statRegexp->pattern();
    //qDebug() << lineReaderMock->getLine(7);

    // Mock pid_stat64_access has stat64 commands in line 7 (ok), 8-13 (not ok), 14,15,21,27,28 (all ok)
    QCOMPARE(statRegexp->exactMatch(lineReaderMock->getLine(7)), true);
    QCOMPARE(statRegexp->exactMatch(lineReaderMock->getLine(10)), true);
    QCOMPARE(statRegexp->exactMatch(lineReaderMock->getLine(27)), true);

    QCOMPARE(statRegexp->exactMatch(lineReaderMock->getLine(6)), false);
    QCOMPARE(statRegexp->exactMatch(lineReaderMock->getLine(23)), false);
    QCOMPARE(statRegexp->exactMatch(lineReaderMock->getLine(16)), false);

    // check positions
    QCOMPARE(statDetector->getPathPos(),2U);
    QCOMPARE(statDetector->getResultPos(),4U);
    QCOMPARE(statDetector->getPidPos(),1U);

    delete lineReaderMock;
    delete statDetector;
}


void TestSyscallDetectorFactory::testExecvDetector()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_plain_big();
    CSyscallDetectorFactory testedFactory = CSyscallDetectorFactory(lineReaderMock);
    CSyscallDetector* execvDetector = testedFactory.createDetector(CSyscallDetectorFactory::execv);
    QVERIFY(0!=execvDetector);

    const QRegExp* execvRegexp = execvDetector->getSyscallRegexp();

    //qDebug() << execvRegexp->pattern();
    //qDebug() << lineReaderMock->getLine(1);

    // Mock plain big has execv command in line 1
    QCOMPARE(execvRegexp->exactMatch(lineReaderMock->getLine(1)), true);
    QCOMPARE(execvRegexp->exactMatch(lineReaderMock->getLine(2)), false);

    // check positions
    QCOMPARE(execvDetector->getPathPos(),1U);
    QCOMPARE(execvDetector->getResultPos(),3U);
    QCOMPARE(execvDetector->getPidPos(),0U);

    delete lineReaderMock;
    delete execvDetector;
}


void TestSyscallDetectorFactory::testAccessDetector()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_pid_stat64_access();
    CSyscallDetectorFactory testedFactory = CSyscallDetectorFactory(lineReaderMock);
    CSyscallDetector* accessDetector = testedFactory.createDetector(CSyscallDetectorFactory::access);
    QVERIFY(0!=accessDetector);

    const QRegExp* accessRegexp = accessDetector->getSyscallRegexp();

    //qDebug() << accessRegexp->pattern();
    //qDebug() << lineReaderMock->getLine(7);

    // Mock pid_stat64_access has access commands in line 20 and 26 (all ok)
    QCOMPARE(accessRegexp->exactMatch(lineReaderMock->getLine(20)), true);
    QCOMPARE(accessRegexp->exactMatch(lineReaderMock->getLine(26)), true);

    QCOMPARE(accessRegexp->exactMatch(lineReaderMock->getLine(6)), false);
    QCOMPARE(accessRegexp->exactMatch(lineReaderMock->getLine(23)), false);
    QCOMPARE(accessRegexp->exactMatch(lineReaderMock->getLine(16)), false);

    // check positions
    QCOMPARE(accessDetector->getPathPos(),2U);
    QCOMPARE(accessDetector->getResultPos(),4U);
    QCOMPARE(accessDetector->getPidPos(),1U);

    delete lineReaderMock;
    delete accessDetector;
}

void TestSyscallDetectorFactory::testSyscallList()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_pid();
    CSyscallDetectorFactory testedFactory = CSyscallDetectorFactory(lineReaderMock);

    std::map<CSyscallDetectorFactory::ESyscallType,QString> availableSyscalls =
            testedFactory.getAvailableSyscalls();

    QCOMPARE(availableSyscalls.size(), static_cast<size_t>(4));
    QCOMPARE(availableSyscalls.count(CSyscallDetectorFactory::open), static_cast<size_t>(1));
    QCOMPARE(availableSyscalls.count(CSyscallDetectorFactory::stat), static_cast<size_t>(1));
    QCOMPARE(availableSyscalls.count(CSyscallDetectorFactory::execv), static_cast<size_t>(1));
    QCOMPARE(availableSyscalls.count(CSyscallDetectorFactory::access), static_cast<size_t>(1));

    QCOMPARE(availableSyscalls[CSyscallDetectorFactory::open], QString("open"));
}


class TestSyscallOccurrences: public QObject
{
    Q_OBJECT
    private slots:
        void testOpen_plain_big();
        void testExecv_plain_big();

};

void TestSyscallOccurrences::testOpen_plain_big()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_plain_big();
    CSyscallDetectorFactory detectorFactory = CSyscallDetectorFactory(lineReaderMock);

    CSyscallDetector* detectorOpen = detectorFactory.createDetector(CSyscallDetectorFactory::open);
    CSyscallOccurrences openOccurrences(lineReaderMock, detectorOpen);
    std::set<unsigned int> openOccurrenceList = openOccurrences.getSyscallOccurrenceLineNumbers();

    // there are 3 occurrences: line 5,9,17
    QCOMPARE(openOccurrenceList.size(), static_cast<size_t>(3));
    QCOMPARE(openOccurrenceList.count(5), static_cast<size_t>(1));
    QCOMPARE(openOccurrenceList.count(9), static_cast<size_t>(1));
    QCOMPARE(openOccurrenceList.count(17), static_cast<size_t>(1));

    delete detectorOpen;
    delete lineReaderMock;
}


void TestSyscallOccurrences::testExecv_plain_big()
{
    CLineReader* lineReaderMock = new CLineReader_StraceMock_plain_big();
    CSyscallDetectorFactory detectorFactory = CSyscallDetectorFactory(lineReaderMock);

    CSyscallDetector* detectorExecv = detectorFactory.createDetector(CSyscallDetectorFactory::execv);
    CSyscallOccurrences execvOccurrences(lineReaderMock, detectorExecv);
    std::set<unsigned int> execvOccurrenceList = execvOccurrences.getSyscallOccurrenceLineNumbers();

    // there is 1 occurrence: line 1
    QCOMPARE(execvOccurrenceList.size(), static_cast<size_t>(1));
    QCOMPARE(execvOccurrenceList.count(1), static_cast<size_t>(1));
    //qDebug() << execvOccurrences.getSyscallPath(1);
    QCOMPARE(execvOccurrences.getSyscallPath(1), static_cast<QString>("/bin/ls"));
    QCOMPARE(execvOccurrences.getSyscallResult(1), static_cast<QString>("0"));


    delete detectorExecv;
    delete lineReaderMock;
}


class TestSyscallOccurrencesLineReader: public QObject
{
    Q_OBJECT
    private slots:
        void testTraceLsVerbose();


};

void TestSyscallOccurrencesLineReader::testTraceLsVerbose()
{
    //@todo trace_ls_reltime geht nicht, da pattern nicht gematcht (nur time, keine pid).
    CLineReader* lineReader = new CLineReader("../../testoutputs/trace_ls.txt");
    CSyscallDetectorFactory detectorFactory = CSyscallDetectorFactory(lineReader);

    CSyscallDetector* detectorOpen = detectorFactory.createDetector(CSyscallDetectorFactory::open);
    CSyscallOccurrences openOccurrences(lineReader, detectorOpen);
    std::set<unsigned int> openOccurrenceList = openOccurrences.getSyscallOccurrenceLineNumbers();

    QCOMPARE(lineReader->getNrOfLines(),118u);
    QCOMPARE(openOccurrenceList.size(), static_cast<size_t>(10));
    QCOMPARE(openOccurrences.getSyscallPath(37), QString("/lib/x86_64-linux-gnu/libc.so.6") );
    QCOMPARE(openOccurrences.getSyscallResult(64), QString("3"));

    /*
    for (unsigned int i=1; i<=lineReader->getNrOfLines();++i)
        qDebug() << lineReader->getLine(i);

    qDebug() << openOccurrenceList.size();

    for (std::set<unsigned int>::iterator curOccurrenceIt = openOccurrenceList.begin();
            curOccurrenceIt != openOccurrenceList.end();
            ++curOccurrenceIt)
    {
        QString curOccuranceLine =  lineReader->getLine(*curOccurrenceIt);
        qDebug() << curOccuranceLine;
    }
    */

    delete detectorOpen;
    delete lineReader;
}

// dont use QTEST_MAIN as we want to use and execute several test classes here 
//QTEST_MAIN(TestSyscallProcessor)
 #include "unittest.moc"


int main()
{
    std::cout << "unit test for syscall processor \n" << std::endl;
    
    
    TestSyscallDetectorFactory test1;
    QTest::qExec(&test1);
    

    TestSyscallOccurrences test2;
    QTest::qExec(&test2);


    TestSyscallOccurrencesLineReader test3;
    QTest::qExec(&test3);

    return(0);
}
