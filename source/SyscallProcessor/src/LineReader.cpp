/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QTextStream>
#include <QFile>
#include <stdexcept>
#include "LineReader.hpp"


CLineReader::CLineReader(QString fileName)
{
    QFile fileHandle(fileName);
    if (!fileHandle.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        throw(std::runtime_error("Cannot open file"));
    }

    size_t nrReadLines = 0;
    const size_t vectorReserveSize = 1000;
    QTextStream inFileStream(&fileHandle);
    while (!inFileStream.atEnd())
    {
        // ic current vector buffer is full,
        // reserve next  vector buffer to avoid speed impacts during push_back
        if (nrReadLines % vectorReserveSize == 0)
        {
            size_t nrAllocatedBlocks = nrReadLines / vectorReserveSize;
            size_t newVecSize = (nrAllocatedBlocks + 1) * vectorReserveSize;
            m_fileContent.reserve(newVecSize);
        }
        QString curLine = inFileStream.readLine();
        if (inFileStream.status() == QTextStream::ReadCorruptData)
        {
            throw(std::logic_error("The file seems to contain non-parseable data"));
        }
        m_fileContent.push_back(curLine);

    }
    fileHandle.close();
}



unsigned int CLineReader::getNrOfLines()
{
    // if a lot of accesses are performed, a member variable may be better
    return(m_fileContent.size());
}



QString CLineReader::getLine(unsigned int lineNr)
{
    if (lineNr > this->getNrOfLines())
    {
        throw(std::out_of_range("Requested line nr not available in file"));
    }

    return(m_fileContent[lineNr-1]);
}
