/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdexcept>
#include <QRegExp>
#include "LineReader.hpp"
#include "SyscallDetector.hpp"
#include "SyscallDetectorFactory.hpp"

//debug only
 #include <QDebug>

// static consts
const QString CSyscallDetectorFactory::m_patternPid="(\\d+)\\s+";
const QString CSyscallDetectorFactory::m_patternTimeOfDay="(\\d\\d:\\d\\d:\\d\\d)\\s";
const QString CSyscallDetectorFactory::m_patternTimeOfDayMicro="(\\d\\d:\\d\\d:\\d\\d)\\.(\\d{6})\\s";
const QString CSyscallDetectorFactory::m_patternTimeOfDayEpoch="(\\d+)\\.(\\d{6})\\s";

const QString CSyscallDetectorFactory::m_patternOpen="open\\(\"([^\"]+)\",\\s(.+)\\)\\s+=\\s(-?\\s?\\d+).*$";
const QString CSyscallDetectorFactory::m_patternStat="stat(?:64)?\\(\"([^\"]+)\",\\s(.+)\\)\\s+=\\s(-?\\s?\\d+).*$";
const QString CSyscallDetectorFactory::m_patternExecv="execve\\(\"([^\"]+)\",\\s(.+)\\)\\s+=\\s(-?\\s?\\d+).*$";
const QString CSyscallDetectorFactory::m_patternAccess="access\\(\"([^\"]+)\",\\s(.+)\\)\\s+=\\s(-?\\s?\\d+).*$";



CSyscallDetectorFactory::CSyscallDetectorFactory(CLineReader* lineReader)
{
    // init all attribs to their default values
    m_hasPidColumn = false; 
    m_hasTimeOfDayCol = false;
    m_hasTimeOfDayMicroCol = false; 
    m_hasTimeOfDayEpochCol = false;
    
    // read the line(s) needed to perform content checks
    QString firstLine;
    try 
    {
        firstLine = lineReader->getLine(1);
    //    qDebug() << firstLine;
    }
    catch (std::out_of_range)
    {
        // throw logic or something if file has not even one line
    }
    
    
    // regex set for detecting the optional strace outpus
    QRegExp regexStracePidLine("^" + m_patternPid + ".*");
    QRegExp regexStraceTimeOfDayCol("^(?:" + m_patternPid + ")?" + m_patternTimeOfDay + ".*");     
    QRegExp regexStraceTimeOfDayMicroCol("^(?:" + m_patternPid + ")?" + m_patternTimeOfDayMicro + ".*"); 
    QRegExp regexStraceTimeOfDayEpochCol("^(?:" + m_patternPid + ")?" + m_patternTimeOfDayEpoch + ".*");
    
    
    if (regexStracePidLine.exactMatch(firstLine)) m_hasPidColumn=true;
    if (regexStraceTimeOfDayCol.exactMatch(firstLine)) m_hasTimeOfDayCol=true;
    if (regexStraceTimeOfDayMicroCol.exactMatch(firstLine)) m_hasTimeOfDayMicroCol=true;
    if (regexStraceTimeOfDayEpochCol.exactMatch(firstLine)) m_hasTimeOfDayEpochCol=true;
    
    // koennte man uu in priv-method-aufrufe umwandeln: setFlagIfMatch(line,pattern,flag)
    // oder flag als returnwert, wurde man dann getFlagIfMatch nennen (imho zu bevorzugen,
    // spart auch init!)
   

}



std::map<CSyscallDetectorFactory::ESyscallType,QString> CSyscallDetectorFactory::getAvailableSyscalls()
{


    // curSyscallMap may also become a member, built by constructor, just returned by this method
    std::map<CSyscallDetectorFactory::ESyscallType,QString> curSyscallMap;

    curSyscallMap.insert(std::make_pair(open,"open"));
    curSyscallMap.insert(std::make_pair(stat,"stat"));
    curSyscallMap.insert(std::make_pair(execv,"execv"));
    curSyscallMap.insert(std::make_pair(access,"access"));

    return(curSyscallMap);
}



CSyscallDetector* CSyscallDetectorFactory::createDetector(ESyscallType syscallType)
{
    switch (syscallType)
    {
    case open:
        return(createDetectorOpen());
    case stat:
        return(createDetectorStat());
    case execv:
        return(createDetectorExecv());
    case access:
        return(createDetectorAccess());
    default:
        Q_ASSERT(false);
    }
}


///@TODO extract a command/path/result method and call this from createOpen/Stat/Execv


CSyscallDetector* CSyscallDetectorFactory::createDetectorOpen()
{
	// pre-command regex memory locations
	unsigned int pidPos = 0;
	unsigned int abstimePos = 0;
	unsigned int epochtimePos = 0;
	unsigned int microsPos = 0;

	// command-specific regex memory locations
	unsigned int pathPos = 0;
	unsigned int modePos = 0;
	unsigned int resultPos = 0;

	// the next available memory location (0=undefined)
	unsigned int currentPos=0;

    QString openRegExpString = getLineStartPattern(currentPos,pidPos,abstimePos,epochtimePos,microsPos);

    openRegExpString+=m_patternOpen;
    pathPos=currentPos;
    ++currentPos;
    modePos=currentPos;
	++currentPos;
	resultPos=currentPos;


	QRegExp* openRegexp = new QRegExp(openRegExpString);
	CSyscallDetector* openDetector = new CSyscallDetector(openRegexp, resultPos, pidPos, pathPos);
	return (openDetector);

}


// shall detect stat() and stat64()
CSyscallDetector* CSyscallDetectorFactory::createDetectorStat()
{
	// pre-command regex memory locations
	unsigned int pidPos = 0;
	unsigned int abstimePos = 0;
	unsigned int epochtimePos = 0;
	unsigned int microsPos = 0;

	// command-specific regex memory locations
	unsigned int pathPos = 0;
	unsigned int modePos = 0;
	unsigned int resultPos = 0;

	// the next available memory location (0=undefined)
	unsigned int currentPos=0;

    QString statRegExpString = getLineStartPattern(currentPos,pidPos,abstimePos,epochtimePos,microsPos);

    statRegExpString+=m_patternStat;
    pathPos=currentPos;
    ++currentPos;
    modePos=currentPos;
	++currentPos;
	resultPos=currentPos;


	QRegExp* statRegexp = new QRegExp(statRegExpString);
	CSyscallDetector* statDetector = new CSyscallDetector(statRegexp, resultPos, pidPos, pathPos);
	return (statDetector);
}


CSyscallDetector* CSyscallDetectorFactory::createDetectorExecv()
{
	// pre-command regex memory locations
	unsigned int pidPos = 0;
	unsigned int abstimePos = 0;
	unsigned int epochtimePos = 0;
	unsigned int microsPos = 0;

	// command-specific regex memory locations
	unsigned int pathPos = 0;
	unsigned int modePos = 0;
	unsigned int resultPos = 0;

	// the next available memory location (0=undefined)
	unsigned int currentPos=0;

    QString execvRegExpString = getLineStartPattern(currentPos,pidPos,abstimePos,epochtimePos,microsPos);

    execvRegExpString+=m_patternExecv;
    pathPos=currentPos;
    ++currentPos;
    modePos=currentPos;
	++currentPos;
	resultPos=currentPos;


	QRegExp* execvRegexp = new QRegExp(execvRegExpString);
	CSyscallDetector* execvDetector = new CSyscallDetector(execvRegexp, resultPos, pidPos, pathPos);
	return (execvDetector);
}



CSyscallDetector* CSyscallDetectorFactory::createDetectorAccess()
{
    // pre-command regex memory locations
    unsigned int pidPos = 0;
    unsigned int abstimePos = 0;
    unsigned int epochtimePos = 0;
    unsigned int microsPos = 0;

    // command-specific regex memory locations
    unsigned int pathPos = 0;
    unsigned int modePos = 0;
    unsigned int resultPos = 0;

    // the next available memory location (0=undefined)
    unsigned int currentPos=0;

    QString accessRegExpString = getLineStartPattern(currentPos,pidPos,abstimePos,epochtimePos,microsPos);

    accessRegExpString+=m_patternAccess;
    pathPos=currentPos;
    ++currentPos;
    modePos=currentPos;
    ++currentPos;
    resultPos=currentPos;


    QRegExp* accessRegexp = new QRegExp(accessRegExpString);
    CSyscallDetector* accessDetector = new CSyscallDetector(accessRegexp, resultPos, pidPos, pathPos);
    return (accessDetector);
}



QString CSyscallDetectorFactory::getLineStartPattern(
		                    unsigned int& nextPos,
		                    unsigned int& pidPos,
                            unsigned int& abstimePos,
                            unsigned int& epochtimePos,
                            unsigned int& microsPos)
{
	// we start at the beginning of the line, so the first available regex memory location is 1,
	// and the regex start is "^"
	unsigned int currentPos=1;
	QString linestartRegExpString="^";

	// check for the possible pre-command line configurations and build the appropriate regex
    if (m_hasPidColumn)
    {
    	linestartRegExpString+=m_patternPid;
    	pidPos=currentPos;
    	++currentPos;
    }
    if (m_hasTimeOfDayCol)
    {
    	linestartRegExpString+=m_patternTimeOfDay;
    	abstimePos=currentPos;
    	++currentPos;
    }
    else if (m_hasTimeOfDayMicroCol)
    {
    	linestartRegExpString+=m_patternTimeOfDayMicro;
    	abstimePos=currentPos;
    	++currentPos;
    	microsPos=currentPos;
    	++currentPos;
    }
    else if (m_hasTimeOfDayEpochCol)
    {
    	linestartRegExpString+=m_patternTimeOfDayEpoch;
    	epochtimePos=currentPos;
    	++currentPos;
    	microsPos=currentPos;
    	++currentPos;
    }

    nextPos=currentPos;
    return(linestartRegExpString);
}


