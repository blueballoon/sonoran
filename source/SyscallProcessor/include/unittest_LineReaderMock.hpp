/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You  should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/


///@file Mockups for LineReader to perform unit-tests without an strace-file

#include <stdexcept>
#include <QString>
#include <vector>
#include "LineReader.hpp"

///@brief Strace mock 1 simulates the first lines of "strace ls"
class CLineReader_StraceMock_plain_big: public CLineReader
{
    public:
        ///@brief constructor for LineReader-StraceMock Nr 1
        CLineReader_StraceMock_plain_big()
        {
            QString currMockLine;
            m_mockLines.reserve(25);

            currMockLine = "execve(\"/bin/ls\", [\"ls\"], [/* 46 vars */]) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "brk(0)                                  = 0x1afb000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "mmap(NULL, 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7fe2d8131000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "access(\"/etc/ld.so.preload\", R_OK)      = -1 ENOENT (No such file or directory)";
            m_mockLines.push_back(currMockLine);
            currMockLine = "open(\"/etc/ld.so.cache\", O_RDONLY)      = 3";
            m_mockLines.push_back(currMockLine);
            currMockLine = "fstat(3, {st_mode=S_IFREG|0644, st_size=103357, ...}) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "mmap(NULL, 103357, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7fe2d8117000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "close(3)                                = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "open(\"/lib64/libselinux.so.1\", O_RDONLY) = 3";
            m_mockLines.push_back(currMockLine);
            currMockLine = "read(3, \"\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0PX \311=\0\0\0\"..., 832) = 832";
            m_mockLines.push_back(currMockLine);
            currMockLine = "fstat(3, {st_mode=S_IFREG|0755, st_size=124624, ...}) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "mmap(0x3dc9200000, 2221912, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x3dc9200000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "mprotect(0x3dc921d000, 2093056, PROT_NONE) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "mmap(0x3dc941c000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1c000) = 0x3dc941c000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "mmap(0x3dc941e000, 1880, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x3dc941e000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "close(3)                                = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "open(\"/lib64/librt.so.1\", O_RDONLY)     = 3";
            m_mockLines.push_back(currMockLine);
            currMockLine = "read(3, \"\177ELF\2\1\1\0\0\0\0\0\0\0\0\0\3\0>\0\1\0\0\0@!\240\310=\0\0\0\"..., 832) = 832";
            m_mockLines.push_back(currMockLine);
            currMockLine = "fstat(3, {st_mode=S_IFREG|0755, st_size=47064, ...}) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "mmap(0x3dc8a00000, 2128816, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x3dc8a00000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "mprotect(0x3dc8a07000, 2093056, PROT_NONE) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "mmap(0x3dc8c06000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x6000) = 0x3dc8c06000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "close(3)                                = 0";
            m_mockLines.push_back(currMockLine);                   
        }
        
        ///@brief destructor
        virtual ~CLineReader_StraceMock_plain_big() {}
        
        ///@brief mock 1 implementation
        virtual unsigned int getNrOfLines()
        {
            return(static_cast<unsigned int>( m_mockLines.size()));
        }
        
        ///@brief mock 1 implementation
        virtual QString getLine(unsigned int lineNr)
        {
            if (lineNr > this->getNrOfLines())
            {
                throw(std::out_of_range("error: requested line nr > available lines"));
            }
            if (0 == lineNr)
            {
                throw(std::out_of_range("error: line nr must be greater than 0"));
            }            
            
            return(m_mockLines[lineNr-1]);
        }
        
    private:
        std::vector<QString> m_mockLines;
};


///@brief Strace mock 2 simulates the first line of "strace -f -o ... bash ls" (pid, no time)
class CLineReader_StraceMock_pid: public CLineReader
{
    public:
        ///@brief constructor for LineReader-StraceMock Nr 2
        CLineReader_StraceMock_pid()
        {
            m_mockLine = "2362  execve(\"/bin/bash\", [\"bash\", \"-c\", \"ls\"], [/* 36 vars */]) = 0";
        }
        
        
        ///@brief destructor
        virtual ~CLineReader_StraceMock_pid() {}
        
        ///@brief mock 1 implementation
        virtual unsigned int getNrOfLines()
        {
            return(static_cast<unsigned int>( 1 ));
        }
        
        ///@brief mock 1 implementation
        virtual QString getLine(unsigned int lineNr)
        {
            if (lineNr > this->getNrOfLines())
            {
                throw(std::out_of_range("error: requested line nr > available lines"));
            }
            if (0 == lineNr)
            {
                throw(std::out_of_range("error: line nr must be greater than 0"));
            }            
            
            return(m_mockLine);
        }
        
    protected:
        QString m_mockLine;
};

///@brief Strace mock 2 simulates the first line of "strace -f -t -o ... bash -c  ls" (pid with abstime)
class CLineReader_StraceMock_pid_abstime: public CLineReader_StraceMock_pid
{
    public:
        CLineReader_StraceMock_pid_abstime()
        {
            m_mockLine = "2384  07:09:58 execve(\"/bin/bash\", [\"bash\", \"-c\", \"ls\"], [/* 36 vars */]) = 0";
        }
        
        virtual ~CLineReader_StraceMock_pid_abstime() {}
        
};


///@brief Strace mock 2 simulates the first line of "strace -f -tt -o ... bash -c  ls" (pid with abstime and micros)
class CLineReader_StraceMock_pid_micros: public CLineReader_StraceMock_pid
{
    public:
        CLineReader_StraceMock_pid_micros()
        {
            m_mockLine = "7440  11:55:14.704442 execve(\"/bin/bash\", [\"bash\", \"-c\", \"ls\"], [/* 36 vars */]) = 0";
        }
        
        virtual ~CLineReader_StraceMock_pid_micros() {}
};


///@brief Strace mock 2 simulates the first line of "strace -f -ttt -o ... bash -c ls" (pid with epochtime and micros)
class CLineReader_StraceMock_pid_epoch: public CLineReader_StraceMock_pid
{
    public:
        CLineReader_StraceMock_pid_epoch()
        {
            m_mockLine = "7460  1375610201.272959 execve(\"/bin/bash\", [\"bash\", \"-c\", \"ls\"], [/* 36 vars */]) = 0";
        }
        
        virtual ~CLineReader_StraceMock_pid_epoch() {}        
};



///@brief this mock has an excerpt of  "strace -f  -o ... bash -c ls" containing stat64-and access-lines and doesnt start at trace line 1
class CLineReader_StraceMock_pid_stat64_access: public CLineReader
{
    public:
        ///@brief constructor for LineReader-StraceMock Nr 1
        CLineReader_StraceMock_pid_stat64_access()
        {
            QString currMockLine;
            m_mockLines.reserve(35);

            currMockLine = "2362  open(\"/usr/lib/i386-linux-gnu/gconv/gconv-modules.cache\", O_RDONLY) = 3";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  fstat64(3, {st_mode=S_IFREG|0644, st_size=26256, ...}) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  mmap2(NULL, 26256, PROT_READ, MAP_SHARED, 3, 0) = 0xb7756000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  close(3)                          = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  brk(0xa053000)                    = 0xa053000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  getppid()                         = 2361";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\".\", {st_mode=S_IFDIR|0775, st_size=4096, ...}) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/usr/lib/lightdm/lightdm/bash\", 0xbfc95d80) = -1 ENOENT (No such file or directory)";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/usr/local/sbin/bash\", 0xbfc95d80) = -1 ENOENT (No such file or directory)";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/usr/local/bin/bash\", 0xbfc95d80) = -1 ENOENT (No such file or directory)";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/usr/sbin/bash\", 0xbfc95d80) = -1 ENOENT (No such file or directory)";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/usr/bin/bash\", 0xbfc95d80) = -1 ENOENT (No such file or directory)";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/sbin/bash\", 0xbfc95d80)  = -1 ENOENT (No such file or directory)";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/bin/bash\", {st_mode=S_IFREG|0755, st_size=920788, ...}) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/bin/bash\", {st_mode=S_IFREG|0755, st_size=920788, ...}) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  geteuid32()                       = 1000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  getegid32()                       = 1000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  getuid32()                        = 1000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  getgid32()                        = 1000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  access(\"/bin/bash\", X_OK)         = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/bin/bash\", {st_mode=S_IFREG|0755, st_size=920788, ...}) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  geteuid32()                       = 1000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  getegid32()                       = 1000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  getuid32()                        = 1000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  getgid32()                        = 1000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  access(\"/bin/bash\", R_OK)         = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/bin/bash\", {st_mode=S_IFREG|0755, st_size=920788, ...}) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  stat64(\"/bin/bash\", {st_mode=S_IFREG|0755, st_size=920788, ...}) = 0";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  geteuid32()                       = 1000";
            m_mockLines.push_back(currMockLine);
            currMockLine = "2362  getegid32()                       = 1000";
            m_mockLines.push_back(currMockLine);
        }

        ///@brief destructor
        virtual ~CLineReader_StraceMock_pid_stat64_access() {}

        ///@brief mock 1 implementation
        virtual unsigned int getNrOfLines()
        {
            return(static_cast<unsigned int>( m_mockLines.size() + 1 ));
        }

        ///@brief mock 1 implementation
        virtual QString getLine(unsigned int lineNr)
        {
            if (lineNr > this->getNrOfLines())
            {
                throw(std::out_of_range("error: requested line nr > available lines"));
            }
            if (0 == lineNr)
            {
                throw(std::out_of_range("error: line nr must be greater than 0"));
            }

            return(m_mockLines[lineNr-1]);
        }

    private:
        std::vector<QString> m_mockLines;
};



