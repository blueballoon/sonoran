#ifndef __LINEREADER__HPP__
#define __LINEREADER__HPP__
/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QString>
#include <vector>


///@brief provides a simple interface to request single lines from a textfile
class CLineReader
{
    public:
        ///@brief constructor
        ///@param fileName the name of the textfile on that the lineReader instance operates
        ///@throw std::runtime_error if the file is cannot be opened for reading (missing, wrong perms)
        ///@throw std::logic_error if the file does not seem to be a textfile 
        CLineReader(QString fileName);
        
        ///@brief destructor
        virtual ~CLineReader() {}
        
        ///@brief reports the number of lines from the associated textfile
        ///@return the number of lines from the associated textfile
        virtual unsigned int getNrOfLines();
        
        ///@brief returns the request line from the associate textfile 
        ///@param lineNr the requeste line nr, starting with 1
        ///@return the requested line as QString
        ///@throw std::out_of_range if lineNr > nr of lines
        virtual QString getLine(unsigned int lineNr);
    
    
    protected:
        ///@brief dummy constructor to enable derived mocks
        CLineReader() {}
    
    private:
        std::vector<QString> m_fileContent;

};

#endif  //__LINEREADER__HPP__
