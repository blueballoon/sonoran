#ifndef __SYSCALLDETECTOR__HPP__
#define __SYSCALLDETECTOR__HPP__
/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/


// cannot use forward decl here because of inlined destructor
#include <QRegExp>


///@brief container for the strace- and syscall-specific regex and its provided capture-positions
class CSyscallDetector
{
    // CSyscallDetector shall only be created by its factory
    friend class CSyscallDetectorFactory;
    
    public:         
        ///@brief destructor, finally deletes the associated reqexp   
        ~CSyscallDetector() { delete m_syscallRegexp; }
        
        ///@brief returns the regular expression
        ///@return const pointer to the regex; note: Syscall Detector instance remains the owner of regex
        const QRegExp* getSyscallRegexp() { return(m_syscallRegexp); }
        
        ///@brief returns then capture position of the result token
        ///@return >1 if a result capture is provided by the regexp, 0 otherwise
        unsigned int getResultPos() { return(m_resultPos); }
        
        ///@brief returns then capture position of the pid token
        ///@return >1 if a pid capture is provided by the regexp, 0 otherwise        
        unsigned int getPidPos() { return(m_pidPos); }
        
        ///@brief returns then capture position of the path/filename token
        ///@return >1 if a path/filename capture is provided by the regexp, 0 otherwise        
        unsigned int getPathPos() { return(m_pathPos); }

        
    private:    // methods
        ///@brief creates a syscall detector with the given regexp and its capture-positions
        CSyscallDetector(QRegExp* syscallRegexp, unsigned int resultPos, 
	    unsigned int pidPos=0, 
	    unsigned int pathPos=0):
            m_syscallRegexp(syscallRegexp),
            m_resultPos(resultPos),
            m_pidPos(pidPos),
            m_pathPos(pathPos) {}    
        
    private:    // attribs
        QRegExp* m_syscallRegexp;
        unsigned int m_resultPos;
        unsigned int m_pidPos;
        unsigned int m_pathPos;
        
};


#endif  //__SYSCALLDETECTOR__HPP__
