#ifndef __SYSCALLOCCURRENCES__HPP__
#define __SYSCALLOCCURRENCES__HPP__
/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <set>
#include <QString>


// forwards
class CSyscallDetector;
class CLineReader;

///@brief provides access to all occurrences of a specific kind of syscall in a strace outputfile
class CSyscallOccurrences
{
    public:
        ///@brief instantiates the desired syscall occurrence handle
        ///@param currStraceReader the handler for the given strace file
        ///@param currSyscallDetector the desired syscall detector (open, access, execv,..)
        CSyscallOccurrences(CLineReader* currStraceReader, CSyscallDetector* currSyscallDetector);
        
        ///@brief determines all occurrences of the (constructor-selected) syscall in the (constructor-selected) strace-file
        ///@return the line numbers (starting at 1) of all syscall occurrences
        std::set<unsigned int> getSyscallOccurrenceLineNumbers();

        ///@brief return the result caption for the given strace line nr
        ///@return the digit-only part of the syscall result, empty string if not available
        ///@throw see getCaptionString
        QString getSyscallResult(unsigned int straceLineNr);

        ///@brief return the path caption for the given strace line nr
        ///@return the complete path that occurs between the corresponding qutation marks, empty string if not available
        ///@throw see getCaptionString
        QString getSyscallPath(unsigned int straceLineNr);
        
        ///@brief return the pid caption for the given strace line nr
        ///@return the digit-only part of the (optional) pid output, empty string if not available
        ///@throw see getCaptionString
        QString getSyscallPid(unsigned int straceLineNr);

    private: // methods
        ///@brief generic method to extract the caption with given position in a given line nr
        ///@param straceLineNr the line number of the strace fiel to be examined, must be member of the getSyscallOccurrenceLineNumbers-set
        ///@param captionPosition teh desired caption position, usually from SyscallDetector.getXXXpos
        ///@return the desired caption string or an empty string if the caption nr is 0
        ///@throw std::invalid_argument if the line nr is not in the getSyscallOccurrenceLineNumbers-set
        ///@throw std::runtime_error if the regex match for the given syscall fails (shall never happen!)
        QString getCaptionString(unsigned int straceLineNr, unsigned int captionPosition);
        
    private: // attribs
        std::set<unsigned int> m_syscallOccurrenceLineNumbers;
        CLineReader* m_straceReader;
        CSyscallDetector* m_syscallDetector;
};


#endif   // __SYSCALLOCCURRENCES__HPP__
