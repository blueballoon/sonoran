#ifndef __SYSCALLDETECTORFACTORY__HPP__
#define __SYSCALLDETECTORFACTORY__HPP__
/*
   Copyright 2013 Alexander Haas
   This file is part of Sonoran

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QString>
#include <map>

// forwards
class CSyscallDetector;
class CLineReader;

///@brief factory to produce the different strace-token-detectors
class CSyscallDetectorFactory
{
    // enable direct access for unit test
    friend class TestSyscallDetectorFactory;

    public:  // data types
    // data types for managing the availabel syscall types
        enum ESyscallType
        {
            open,
            stat,
            execv,
            access
        };

    public:
        CSyscallDetectorFactory(CLineReader* lineReader);
        std::map<CSyscallDetectorFactory::ESyscallType,QString> getAvailableSyscalls();
        CSyscallDetector* createDetector(ESyscallType syscallType);

    private:  // methods
        CSyscallDetector* createDetectorOpen();
        CSyscallDetector* createDetectorStat();
        CSyscallDetector* createDetectorExecv();
        CSyscallDetector* createDetectorAccess();
        // to be extended later ...

        ///@brief depending on pid and time settings, the chars before command differ and needs a suiteable regex
        ///@param [out] nextPos set the available regex memory location; always >= 1
        ///@param [out] pidPos set the regex memory location for pid, if applicable; or to 0 otherwise
        ///@param [out] abstimePos set the regex memory location for abstime, if applicable; or to 0 otherwise
        ///@param [out] epochtimePos set the regex memory location for epochtime, if applicable; or to 0 otherwise
        ///@param [out] microsPos set the regex memory location for microseconds, if applicable; or to 0 otherwise
        ///@return the regex string that matches the current line start as far as the command itself starts
        QString getLineStartPattern(unsigned int& nextPos,
        		                    unsigned int& pidPos,
    	                            unsigned int& abstimePos,
    	                            unsigned int& epochtimePos,
    	                            unsigned int& microsPos);

        
    private:  // attribs
        static const QString m_patternPid;
        static const QString m_patternTimeOfDay;
        static const QString m_patternTimeOfDayMicro;
        static const QString m_patternTimeOfDayEpoch;
        static const QString m_patternOpen;
        static const QString m_patternStat;
        static const QString m_patternExecv;
        static const QString m_patternAccess;
    
    bool m_hasPidColumn;
    bool m_hasTimeOfDayCol;   // time of day, std format (hh:mm:ss)
    bool m_hasTimeOfDayMicroCol; // time of day, std format + micros (hh:mm:ss.uuuuuu)
    bool m_hasTimeOfDayEpochCol; // time of day, seconds since epoch start + micros (ssssssssss.uuuuuu)
	
};


#endif   // __SYSCALLDETECTORFACTORY__HPP__
